window.addEventListener('DOMContentLoaded', async () => {
    // const url = 'http://localhost:8000/api/conferences/';
    // const response = await fetch(url);
    // console.log(response);

    // const data = await response.json();

    const url = 'http://localhost:8000/api/conferences/';
    function createCard(name, description, pictureUrl,start_date,end_date,location) {
        return `
        <div class="grid gap-3 col-4">
          <div class="card ">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body shadow p-3 mb-5 bg-body-tertiary rounded">
              <h5 class="card-title">${name}</h5>
              <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
              <p class="card-text ">${description}</p>
            </div>
            <div class="card-footer">${start_date}-${end_date}</div>
          </div>
        </div>
        `;
      }
//                   <div class="card-footer">${start_date}-${end_date}</div>
    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error("Error" + response.status)
      } else {
        const data = await response.json();

        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // // nameTag.innerHTML = conference.name;

        // const detailUrl = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch(detailUrl);
        // if (detailResponse.ok) {
        //   const details = await detailResponse.json();

        //   const description = details.conference.description;
        //   const detailTag = document.querySelector('.card-text');
        //   detailTag.innerHTML = description;

        //   const imageTag = document.querySelector('.card-img-top');
        //   imageTag.src = details.conference.location.picture_url;

        // }
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const start_date = new Date(details.conference.starts).toLocaleDateString();
              const end_date= new Date(details.conference.ends).toLocaleDateString();
              const location = details.conference.location.name
              const html = createCard(title, description, pictureUrl,start_date,end_date,location);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
        }
        }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error('Error',e);
    }

  });
